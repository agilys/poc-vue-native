export default interface Banner {
    id: string;
    campaignName: string;
    startDate: Date;
    endDate?: Date;
    status?: string;
    position?: string;
    mobile: {
        displayed: boolean
    };
    web: {
        displayed: boolean
        visibleOnHomePage: boolean
    };
    clubId: string;
    bannerZones: Array<{
        zone: {
            id: string,
            name: string
        }
        imageUrl: string,
        title: string,
        description: string,
        url: string
    }>;
}
