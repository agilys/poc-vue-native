export interface Authentication {
  authenticated: boolean;
  accessToken?: string;
  idToken?: string;
  expiresIn?: string;
  idTokenPayload?: any;
}
