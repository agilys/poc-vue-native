import Vue from 'vue';
import Vuex from 'vuex';

import AuthenticationStore from './modules/authentication/AuthenticationStore';
import RootState from './modules/RootState';
import BannerStore from './modules/banners/BannerStore';

Vue.use(Vuex);

export const createStore = () => new Vuex.Store<RootState>({
  modules: {
    // add all state modules here !
    AuthenticationStore,
    BannerStore
  }
});

const store = createStore();

export default store;

// https://github.com/DevoidCoding/TodoMVC-Vue-TypeScript
// https://github.com/vuejs/vuex/issues/564

