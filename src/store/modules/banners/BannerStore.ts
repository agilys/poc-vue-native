import { MutationTree, Getter, GetterTree } from 'vuex';
import $store from '../../index';
import BannerService from '@/services/banners/BannerService';
import Banner from '@/models/banner/Banner';
import { rootState, BannersState } from '../RootState';

const bannerService = new BannerService();
const state: BannersState = rootState.bannersState;

const getters = {
  banners(): Banner[] {
    return state.banners;
  }
};

const mutations: MutationTree<BannersState> = {
  setBanners(store: BannersState, payload: Banner[]) {
    state.banners = [...store.banners, ...payload];
  }
};

const actions = {
  getBanners() {
    const result = bannerService
      .getBanners()
      .then((r) => {
        $store.commit('setBanners', r);
      });

      // const result = await bannerService.getBanners();
      // $store.commit('setBanners', result);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
