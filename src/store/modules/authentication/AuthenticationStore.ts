import Auth0Service from '@/services/authentication/Auth0Service';
import { MutationTree, Getter, GetterTree } from 'vuex';
import $store from '../../index';
import { Authentication } from '../../../models/authentication/Authentication';
import { rootState, AuthenticationState, rootAuthenticationState } from '../RootState';

const authService = new Auth0Service();
const state: AuthenticationState = rootState.authenticationState;

const getters = {
  isAuthenticated(): boolean {
    return state.authentication.authenticated;
  }
};

const mutations: MutationTree<AuthenticationState> = {
  authenticated(store: AuthenticationState, payload: Authentication) {
    store.authentication = { ... payload, authenticated: true};
  },
    logout(store: AuthenticationState, payload: Authentication) {
    // TODO : spread operator does not work here
    // store.authentication = { ...rootAuthenticationState.authentication };
    store.authentication.authenticated = false;
  }
};

const actions = {
  login() {
    authService.login();
  },
  logout() {
    const result = authService.logout();
    result.then(() => {
      $store.commit('logout');
    });
  },
  handleAuthentication() {
    const result = authService.handleAuthentication();
    result.then((authResult) => {
      $store.commit('authenticated', authResult);
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
