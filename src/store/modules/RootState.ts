import { Authentication } from '../../models/authentication/Authentication';
import Banner from '@/models/banner/Banner';

export const rootAuthenticationState: AuthenticationState = {
  authentication: {
    authenticated: false,
    accessToken: '',
    idToken: '',
    expiresIn: '',
    idTokenPayload: ''
  }
};

export const rootBannersState: BannersState = {
  banners: []
};

export interface AuthenticationState {
  authentication: Authentication;
}

export interface BannersState {
  banners: Banner[];
}

export default interface RootState {
  authenticationState: AuthenticationState;
  bannersState: BannersState;
}

export const rootState: RootState = {
  authenticationState: rootAuthenticationState,
  bannersState: rootBannersState
};
