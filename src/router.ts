import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/Home.vue';
import Callback from './pages/auth0/Callback.vue';
import About from './pages/About.vue';
import ListView from './pages/banners/ListView.vue';
import $store from './store/index';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/banners',
      name: 'Banners',
      component: ListView
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/callback',
      name: 'callback',
      component: Callback
    },
  ]
});

router.beforeEach((to, from, next) => {
  if (to.name === 'callback') { // check if "to"-route is "callback" and allow access
    next();
  } else if ($store.getters.isAuthenticated) { // if authenticated allow access
    next();
  } else if (from.name !== 'callback') {
    $store.dispatch('login');
  }
});

export default router;
