import Banner from '@/models/banner/Banner';

export default class BannerService {
    // private graphQlClient: auth0.WebAuth;

    constructor() {
        // graphQlClient = new .target.target..();
    }

    public getBanners(): Promise<Banner[]> {

        const result: Banner[] = [
            {
                id: '1',
                startDate: new Date('2017-06-25'),
                mobile: {
                    displayed: false
                },
                clubId: '1',
                web: {
                    displayed: true,
                    visibleOnHomePage: true
                },
                campaignName: 'blabla',
                bannerZones: new Array({
                    zone: {
                        id: '13',
                        name: 'someZone'
                    },
                    imageUrl: '',
                    title: 'someTitle',
                    description: 'someDescription',
                    url: 'someUrl'
                })
            } as Banner,
            {
                id: '2',
                startDate: new Date('2017-06-25'),
                mobile: {
                    displayed: false
                },
                clubId: '1',
                web: {
                    displayed: true,
                    visibleOnHomePage: true
                },
                campaignName: 'blabla',
                bannerZones: new Array({
                    zone: {
                        id: '13',
                        name: 'someZone'
                    },
                    imageUrl: '',
                    title: 'someTitle',
                    description: 'someDescription',
                    url: 'someUrl'
                })
            } as Banner,
        ];

        return Promise.resolve(result);
        // return new Promise((resolve, reject) => {
        //     this.graphQlClient.parseHash((err, authResult) => {
        //       if (err) {
        //         return reject(err);
        //       }
        //       return resolve(authResult);
        //     });
        // });
    }
}
