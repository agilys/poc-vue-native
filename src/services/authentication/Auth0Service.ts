import auth0 from 'auth0-js';

export default class Auth0Service {
  private auth0: auth0.WebAuth;
  constructor() {
    this.auth0 = new auth0.WebAuth({
      clientID: 'Cn7V4kczHODeDU3QnFVi8wCXciogYFwP',
      domain: 'myshopi.eu.auth0.com',
      responseType: 'token id_token',
      audience: 'https://backend-api.myshopi.com',
      scope: 'read:webshop-promos read:clubs read:banners create:banners update:banners openid profile',
      redirectUri: 'http://localhost:3000' + '/callback'
    });
  }

  public login() {
    this.auth0.authorize();
  }

  public handleAuthentication(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (err) {
          return reject(err);
        }
        return resolve(authResult);
      });
    });
  }

  public logout(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.auth0.logout({
        returnTo : ''
      });

      return resolve();
    });
  }
}
